FROM openjdk:12
ADD target/docker-spring-boot-luisvidaletti.jar docker-spring-boot-luisvidaletti.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-luisvidaletti.jar"]
